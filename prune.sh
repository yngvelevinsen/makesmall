git filter-branch --prune-empty --tree-filter '
git lfs track "*.dst" "*.rar" "*.zip" "*.[be]d[xyz]" "*.pdf" "*.key" "*.vane" "tracelx64" > /dev/null
git add .gitattributes
git ls-files | xargs -d "\n" git check-attr filter | grep "filter: lfs" | sed -r "s/(.*): filter: lfs/\1/" | xargs -d "\n" -r -n 50 bash -c "git rm -f --cached \"\$@\"; git add \"\$@\"" bash \
' --tag-name-filter cat -- --all
