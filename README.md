## To get small ESS lattice repo

 - clone current
 - run git lfs install
 - check out the branches you want
 - for each branch, run prune.sh to rewrite all commits
 - push to new repository (currently must use https, not ssh)
