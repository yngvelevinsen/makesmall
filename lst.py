hashes={}
for l in file('allfileshas.txt','r'):
    lsp=l.split()
    fname=l[len(lsp[0]):].strip()
    hashes[lsp[0]]=fname

deletes=file('bigtosmall.txt','w')
for f in file('bigobjects.txt','r'):
    hsh=f.split()[0]
    size=float(f.split()[2])/1024/1024 # in MB
    #print hashes[hsh],size
    if size<20.0:
        break
    else:
        print "git filter-branch --prune-empty --index-filter 'git rm -rf --cached --ignore-unmatch {0} && git add {0}' --tag-name-filter cat -- --all".format(hashes[hsh])
